"use client"

import { useEffect } from "react"

export default function Page2() {
    useEffect(() => {
        const evtSource = new EventSource("/api/foo");

        evtSource.onmessage = (event: MessageEvent<any>) => console.log(event)
    }, [])

    return <></>
}