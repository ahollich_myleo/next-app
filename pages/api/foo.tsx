
import type { NextApiRequest, NextApiResponse } from 'next'
// import * as AWS from "@aws-sdk/client-sqs";
// const client = new AWS.SQS({region: "eu-central-1"});
 
type ResponseData = {
  message: string
}
 
export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>
) {

  res.setHeader("Cache-Control", "no-store");
  res.setHeader("Content-Type", "text/event-stream")
  res.setHeader('Connection', 'keep-alive');
  res.flushHeaders(); // flush the headers to establish SSE with client

  const intVal = setInterval(() => {
    // const msg = await client.receiveMessage({
    //   QueueUrl: "https://sqs.eu-central-1.amazonaws.com/088366992753/next-js-queue",
    // });

    res.write(`data: ${JSON.stringify("msg")}\n\n`);
   
  }, 1_000);


  res.on('close', () => {
    console.log('client dropped me');
    clearInterval(intVal);
    res.end();
});
}